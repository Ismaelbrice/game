let container = document.querySelector <HTMLElement> (".container") ;
let btn = document.querySelector <HTMLButtonElement> (".btn") ;
let scores = document.querySelector <HTMLElement> (".score") ;
let temps = document.querySelector <HTMLElement> (".temps") ;

btn.onclick = function () {
  let score = 0 ;
  let time = 15 ;
  container.innerHTML = "" ;

  let interval = setInterval (function showTarget() {
    let target = document.createElement ("img") ;
    target.id = "target" ;
    target.src = "/tetedem.png" ;
    container.appendChild(target) ;
    target.style.top = Math.random() * (500 - target.offsetHeight) + "px" ;
    target.style.left = Math.random () * (600 - target.offsetWidth) + "px" ;

    setTimeout (function () {
      target.remove () ;
    } , 2000 )
  

  target.onclick = function () {
    score ++ ;
    target.style.display = "none" ;
  }
  time -= 1 ;

  scores.innerHTML = `score : ${score}` ;
  temps.innerHTML = `temps : ${time}` ;

  if (time == 0) {
    clearInterval (interval) ;
    container.innerHTML = "jeu terminé" ;
  } 
} ,1000) ;
}
